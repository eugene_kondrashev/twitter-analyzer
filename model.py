# coding=utf-8
'''
Created on Jul 10, 2014

@author: ekondrashev
'''
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

from sqlalchemy import Column, BigInteger, Integer, String


class IdToInReplyToStatusId(Base):
    __tablename__ = 'id_to_in_reply_to_status_id'
    id = Column(Integer, primary_key=True)
    parent_id = Column(BigInteger)
    in_reply_to_status_id = Column(BigInteger)

    def __repr__(self):
        return "<IdToInReplyToStatusId(parent_id='%s', in_reply_to_status_id='%s')>" % (
                        self.parent_id, self.in_reply_to_status_id)

# class User(Base):
#     __tablename__ = 'users'
#     id = Column(Integer, primary_key=True)
#     name = Column(String)
#     fullname = Column(String)
#     password = Column(String)
#
#     def __repr__(self):
#         return "<User(name='%s', fullname='%s', password='%s')>" % (
#                         self.name, self.fullname, self.password)