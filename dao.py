# coding=utf-8
'''
Created on Jul 10, 2014

@author: ekondrashev
'''
import model
import logging


def get_impl():
    from sqlalchemy import create_engine
    from sqlalchemy.orm import sessionmaker

    engine = create_engine('sqlite:////tmp/twitter.db', echo=True)
    model.Base.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)
    return Dao(engine, Session())


class Dao(object):
    COMMIT_PERIOD = 100
    def __init__(self, engine, session):
        self.engine = engine
        self.session = session
        self.count = 0

    def __add(self, obj):
        self.session.add(obj)
        self.count = self.count + 1
        logging.info('Count %d:' % self.count)
        if self.count >= self.COMMIT_PERIOD:
            self.count = 0
            self.session.commit()

    def _save_comment_raltion(self, comment):
        relation = model.IdToInReplyToStatusId(parent_id=comment.id, in_reply_to_status_id=comment.in_reply_to_status_id)
        self.__add(relation)

    def save_comment(self, comment):
        self._save_comment_raltion(comment)
