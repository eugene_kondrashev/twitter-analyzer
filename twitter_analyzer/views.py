from django.http import HttpResponse
from django.template import RequestContext, loader


def analyze_user_timeline(request, user_id, count):
    template = loader.get_template('timeline.html')
    context = RequestContext(request, {
        'user_id': user_id,
        'count': count,
    })
    return HttpResponse(template.render(context))

