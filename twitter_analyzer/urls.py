from django.conf.urls import patterns, include, url

from django.contrib import admin
from twitter_analyzer.views import analyze_user_timeline
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'twitter_analyzer.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^rest/', include('rest.urls')),
    url(r'^analyze_user_timeline/(?P<user_id>.+?)/(?P<count>[0-9]+)$', analyze_user_timeline, name='analyze_user_timeline'),
)
