# coding=utf-8
'''
Created on Sep 3, 2014

@author: ekondrashev
'''
import logging


class Impl(object):

    @staticmethod
    def get(*args, **kwargs):
        from emv.aminstitute import engine
        return engine.Impl()

    def analyze(self, text):
        raise NotImplementedError('analyze')

    def analyze_comments(self, comments):
        result = []
        failed_comments = []
        for comment in comments:
            emv_ = self.analyze(comment.text.strip())
            if emv_ is not None:
                result.append(emv_)
            else:
                logging.info("Failed to calculate emv for '%s'" % comment.text)
                failed_comments.append(comment)
        return float(sum(result)) / len(result), failed_comments
