# coding=utf-8
'''
Created on Jul 10, 2014

@author: ekondrashev
'''
import urllib
import urllib2
import re
import logging
from emv.analyze import engine

'''
<option value="Uncategorized">--Select a Category--</option>
                    <option>Arts & Entertainment</option>
                    <option>Automotive</option>
                    <option>Business &amp; Professional Services</option>
                    <option>Clothing & Accessories</option>
                    <option>Community & Government</option>
                    <option>Computers & Electronics</option>
                    <option>Construction & Contractors</option>
                    <option>Education</option>
                    <option>Food & Dining</option>
                    <option>Health & Medicine</option>
                    <option>Home & Garden</option>
                    <option>Industry & Agriculture</option>
                    <option>Legal & Financial</option>
                    <option>Media & Communications</option>
                    <option>Personal Care & Services</option>
                    <option>Real Estate</option>
                    <option>Shopping</option>
                    <option>Sports & Recreation</option>
                    <option>Travel & Transportation</option>

'''

URL = r'http://www.aminstitute.com/cgi-bin/headline.cgi'


RE = 'total\sof\s(\d\d\.\d\d)%'


def parse_emv(html):
    m = re.search(RE, html)
    if m:
        return float(m.group(1)) / 100
    else:
        logging.debug('*' * 50)
        logging.debug("Response: %s" % html)
        logging.debug('*' * 50)


class Impl(engine.Impl):

    def analyze(self, text, category='Media & Communications'):
        text = unicode(text).encode('utf-8')
        values = dict(text=text, category=category)
        data = urllib.urlencode(values)
        req = urllib2.Request(URL, data)
        rsp = urllib2.urlopen(req)
        content = rsp.read()
        return parse_emv(content)
