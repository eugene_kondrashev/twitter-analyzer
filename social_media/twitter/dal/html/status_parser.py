# coding=utf-8
'''
Created on Jul 10, 2014

@author: ekondrashev
'''
import re
from lxml import html
from social_media.model import Message, User
import logging


def __clean_parent_comments(comments, parent_id):
    for i, (user, comment) in enumerate(comments):
        if comment.id == str(parent_id):
            return comments[i + 1:]
    return comments


def parse_in_reply_to_message(htmltext, parent_id, id_):
    if id_ == '487133660705849345':
        print id_
    views_count = 0
    htmltree = html.fromstring(htmltext)
    favorites_count = parse_favorites_count(htmltext)
    retweet_count = parse_retweet_count(htmltext)

    comments = parse_comments(htmltree)
    comments = __clean_parent_comments(comments, parent_id)
    #the first is in reply to comment
    #second is target message
#     user, message = comments[1]
    text = parse_text(htmltree)

    return Message(id_, text, None, views_count, favorites_count, retweet_count, comments)


def parse_message(htmltext, id_):
    views_count = 0
    htmltree = html.fromstring(htmltext)
    favorites_count = parse_favorites_count(htmltext)
    retweet_count = parse_retweet_count(htmltext)
    text = parse_text(htmltree)
    comments = parse_comments(htmltree)
    return Message(id_, text, None, views_count, favorites_count, retweet_count, comments)


def parse_text(htmltree):
    node = htmltree.xpath('//div[@class="permalink-inner permalink-tweet-container"]//p[@class="js-tweet-text tweet-text"]')[0]
    text = node.text_content()
    if not text:
        text = node.text
    if text:
        return clean_at_usernames(text.strip())
    logging.debug('Failed to parse text')


def clean_at_usernames(content):
    return re.sub('@.+?\s', '', content)


def parse_comments(htmltree):
    result = []

    contents = htmltree.xpath('//div[@class="content"]')
    for c in contents:
        user_id = c.xpath('div[@class="stream-item-header"]/a[@class="account-group js-account-group js-action-profile js-user-profile-link js-nav"]/@data-user-id')[0]
        text = c.find('p').text_content()
        text = clean_at_usernames(text)
        details = c.xpath('div[@class="stream-item-footer clearfix"]/a[@class="details with-icn js-details"]/@href')[0]
        m = re.match('/(.+)?/status/(.+)', details)
        if m:
            username, id_ = m.groups()
            result.append((User(user_id, username), Message(id_, text, None, None, None, None, None)))
    return result


def parse_retweet_count(html):
    p = '"Retweeted\s(\d+)\stimes?"'
    m = re.search(p, html)
    if m:
        return int(m.group(1))
    return 0


def parse_favorites_count(html):
    p = '"Favorited\s(\d+)\stimes?"'
    m = re.search(p, html)
    if m:
        return int(m.group(1))
    return 0
