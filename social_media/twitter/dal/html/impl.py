# coding=utf-8
'''
Created on Sep 3, 2014

@author: ekondrashev
'''

from social_media.twitter.dal import impl as dal_impl
import urllib2
from social_media.twitter.dal.html.status_parser import parse_message,\
    parse_in_reply_to_message


class Impl(dal_impl.Impl):

    def _get_html(self, username, id_):
        URL = 'https://twitter.com/'
        QUERY = '%s/status/%s' % (username, id_)
        html_status = urllib2.urlopen(''.join((URL, QUERY))).read()
        return html_status

    def get_in_reply_to_status(self, processed_comments, parent_id, username, id_):
        html = self._get_html(username, id_)
        message = parse_in_reply_to_message(html, parent_id, id_)
        comments = []
        if message.comments:
            for user, comment in message.comments:
                if (user.id, comment.id) not in processed_comments:
                    comment_ = self.get_in_reply_to_status(processed_comments, id_, user.name, comment.id)
                    comments.append((user, comment_))
                    processed_comments[(user.id, comment.id)] = (user, comment)
            message = message._replace(comments=comments)
        message = message._replace(created_at=self.get_created_at(id_))
        return message

    def get_status(self, username, id_):
        html = self._get_html(username, id_)
        message = parse_message(html, id_)
        processed_comments = {}
        comments = []
        if message.comments:
            for user, comment in message.comments:
                if (user.id, comment.id) not in processed_comments:
                    comment_ = self.get_in_reply_to_status(processed_comments, id_, user.name, comment.id)
                    comments.append((user, comment_))
            message = message._replace(comments=comments)
        message = message._replace(created_at=self.get_created_at(id_))
        return message
