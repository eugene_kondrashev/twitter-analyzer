# coding=utf-8
'''
Created on Sep 3, 2014

@author: ekondrashev
'''


def get(*args, **kwargs):
    from social_media.twitter.dal.html import impl as html
    from social_media.twitter.dal.tweepy import impl as tweepy

    class C(html.Impl, tweepy.Impl):
        def __init__(self, *args_, **kwargs_):
            html.Impl.__init__(self, *args_, **kwargs_)
            tweepy.Impl.__init__(self, *args_, **kwargs_)
    return C(*args, **kwargs)


class Impl(object):
    @staticmethod
    def get(*args, **kwargs):
        from social_media.twitter.dal.html import impl
        return impl.Impl()

    def get_created_at(self, id_):
        raise NotImplementedError('get_created_at')

    def get_status(self, username, id_):
        raise NotImplementedError('get_status')

    def get_statuses(self, id_, count):
        raise NotImplementedError('get_statuses')
