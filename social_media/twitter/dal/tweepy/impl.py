# coding=utf-8
'''
Created on Sep 8, 2014

@author: ekondrashev
'''
import tweepy
from social_media.twitter.dal import impl as dal_impl
import _creds


class Impl(dal_impl.Impl):
    def __init__(self):
        username = _creds.username
        password = _creds.password
        auth = tweepy.OAuthHandler(username, password)
        access_token = _creds.access_token
        access_secret = _creds.access_secret
        auth.set_access_token(access_token, access_secret)

        self.api = tweepy.API(auth)

    def get_created_at(self, id_):
        return self.api.get_status(id=id_).created_at

    @staticmethod
    def get(*args, **kwargs):
        return Impl()

    def get_statuses(self, id_, count):
        result = self.api.user_timeline(id=id_, count=count)
        result.reverse()
        return result
