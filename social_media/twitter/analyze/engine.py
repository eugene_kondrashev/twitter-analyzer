# coding=utf-8
'''
Created on Sep 3, 2014

@author: ekondrashev
'''
from social_media.analyze import engine
from social_media.twitter.dal import impl as dal_impl
from social_media import model


class Impl(engine.Impl):

    def __init__(self, emv_engine, dal=None):
        engine.Impl.__init__(self, emv_engine)
        self.dal = dal or dal_impl.get()

    def analyze_status(self, status):
        if status.in_reply_to_status_id:
            status_ = self.dal.get_in_reply_to_status({}, str(status.in_reply_to_status_id), status.user.screen_name, status.id)
            result = self.analyze_message(model.User(None, status.user.screen_name), status_)
        else:
            result = self.analyze_message_by_id(status.user.screen_name, status.id)
        return result

    def analyze_message_by_id(self, username, id_):
        status = self.dal.get_status(username, id_)
        return self.analyze_message(model.User(None, username), status)

    def analyze_user_timeline(self, id_, count):
        timeline = []
        statuses = self.dal.get_statuses(id_, count)
        original_count = len(statuses)
        statuses = [status for status in statuses if not status.text.lower().startswith('rt @')]
        for status in statuses:
            result = self.analyze_status(status)
            timeline.append(result)
        return timeline, original_count - len(statuses)
