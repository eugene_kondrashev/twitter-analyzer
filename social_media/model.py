# coding=utf-8
'''
Created on Sep 3, 2014

@author: ekondrashev
'''
from collections import namedtuple

User = namedtuple('User', 'id name')
Message = namedtuple('Message', 'id text created_at views likes shares comments')
