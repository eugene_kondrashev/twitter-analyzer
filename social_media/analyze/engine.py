# coding=utf-8
'''
Created on Sep 3, 2014

@author: ekondrashev
'''

VIEW_FACTOR = 0.1
LIKE_FACTOR = 1
SHARE_FACTOR = 2
MESSAGE_FACTOR = 2.5
COMMENTS_FACTOR = 3


class Impl(object):

    def __init__(self, emv_engine):
        self.emv = emv_engine

    @staticmethod
    def get(*args, **kwargs):
        emv_engine = kwargs.get('emv_engine')
        if not emv_engine:
            from emv.aminstitute.engine import Impl as aminstitude_emv_engine_impl
            emv_engine = aminstitude_emv_engine_impl()
        from social_media.twitter.analyze.engine import Impl as twitter_analyze_engine_impl
        return twitter_analyze_engine_impl(emv_engine)

    def analyze_message(self, user, message):
        non_analyzed_comments = []
        message_text_emv = self.emv.analyze(message.text)
        if not message_text_emv:
            message_text_emv = 0
            non_analyzed_comments.append((user, message))

        basic_factor = VIEW_FACTOR * message.views + LIKE_FACTOR * message.likes + SHARE_FACTOR * message.shares + MESSAGE_FACTOR * message_text_emv
        if not message.comments:
            return basic_factor, message, non_analyzed_comments
        comment_factors = []
        for user, comment in message.comments:
            comment_factor, _, non_analyzed_comments_ = self.analyze_message(user, comment)
            comment_factors.append(comment_factor)
            non_analyzed_comments.extend(non_analyzed_comments_)

        comments_emv = sum(comment_factors) / len(comment_factors)
        return basic_factor + COMMENTS_FACTOR * comments_emv, message, non_analyzed_comments

    def analyze_message_by_id(self, username, id_):
        raise NotImplementedError('analyze_mesage_by_id')

    def analyze_user_timeline(self, id_, count):
        raise NotImplementedError('analyze_user_timeline')

