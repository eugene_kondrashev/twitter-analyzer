import json
import time
from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
import models
import serializers

import datetime
import distutils.util
from emv.analyze import engine as emv_engine
# Create your views here.


def _is_force(request):
    if u'force' in request.QUERY_PARAMS:
        return distutils.util.strtobool(request.QUERY_PARAMS[u'force'][0])
    return False


def _get_twitter_analyzer(algorithm):
    emv_impl = emv_engine.Impl.get()
    from social_media.twitter.analyze.engine import Impl as twitter_analyze_engine_impl
    return twitter_analyze_engine_impl(emv_impl)


def calculate_total_comments_len(message):
    if not message.comments:
        return 1

    result = []
    for user, comment in message.comments:
        result.append(calculate_total_comments_len(comment))

    return sum(result) + 1


def analyze_and_save_tweet(username, id_, algorithm='default'):
    start_time = datetime.datetime.now()
    result = _get_twitter_analyzer(algorithm).analyze_message_by_id(username, id_)
    result, message, failed_comments = result
    total_comments_count = calculate_total_comments_len(message)
    end_time = datetime.datetime.now()

    comment_analysis_accuracy = 100 - (len(failed_comments) * 100) / total_comments_count
    report = models.TweetAnalyzeReport(start_time=start_time, end_time=end_time, tweet_id=id_, algorithm=algorithm, result=result,
                                       total_comments_count=total_comments_count,
                                       non_analyzed_comments_count=len(failed_comments),
                                       comment_analysis_accuracy=comment_analysis_accuracy)
    report.save()
    return report


@api_view(['GET', ])
def analyze_tweet(request, user_id, id_):
    if request.method == 'GET':
        report = None
        serializer = None
        if _is_force(request):
            report = analyze_and_save_tweet(user_id, id_)
            serializer = serializers.TweetReport(report)
        else:
            try:
                report = models.TweetAnalyzeReport.objects.filter(tweet_id=id_)
                serializer = serializers.TweetReport(report, many=True)
            except models.TweetAnalyzeReport.DoesNotExist:
                report = analyze_and_save_tweet(user_id, id_)
                serializer = serializers.TweetReport(report)
        return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def analyze_and_save_user(user_id, count, algorithm='default'):
    start_time = datetime.datetime.now()
    results = _get_twitter_analyzer(algorithm).analyze_user_timeline(user_id, count)
    timeline, total_retweets_count = results
    emv = []
    failed_comments = []
    total_comments_count = 0
    for result in timeline:
        emv_, message, failed_comments_ = result
        print message.id, emv_
        print failed_comments_
        emv.append(emv_)
        failed_comments.extend(failed_comments_)
        total_comments_count_ = calculate_total_comments_len(message)
        total_comments_count = total_comments_count + total_comments_count_
    end_time = datetime.datetime.now()

    emv = sum(emv) / len(emv)
    comment_analysis_accuracy = 100 - (len(failed_comments) * 100) / total_comments_count
    report = models.UserAnalyzeReport(start_time=start_time, end_time=end_time, user_id=user_id, algorithm=algorithm, result=emv,
                                       total_tweets_count=count,
                                       non_analyzed_comments_count=len(failed_comments),
                                       total_retweets_count=total_retweets_count,
                                       total_comment_analysis_accuracy=comment_analysis_accuracy)
    report.save()
    return report


def analyze_user_timeline_(user_id, count, algorithm='default'):
    start_time = datetime.datetime.now()
    results = _get_twitter_analyzer(algorithm).analyze_user_timeline(user_id, count)
    timeline, total_retweets_count = results

    emv_timeline = []
    emvs = []
    failed_comments = []
    total_comments_count = 0
    for result in timeline:
        emv_, message, failed_comments_ = result
        print message
        print message.id, message.created_at, emv_
        print failed_comments_
        emvs.append(emv_)

        current_avg_emv = sum(emvs) / len(emvs)
        emv_timeline.append([message.created_at, current_avg_emv])
        failed_comments.extend(failed_comments_)
        total_comments_count_ = calculate_total_comments_len(message)
        total_comments_count = total_comments_count + total_comments_count_
#     end_time = datetime.datetime.now()
#
#     emvs = sum(emvs) / len(emvs)
#     comment_analysis_accuracy = 100 - (len(failed_comments) * 100) / total_comments_count
#     report = models.UserAnalyzeReport(start_time=start_time, end_time=end_time, user_id=user_id, algorithm=algorithm, result=emvs,
#                                        total_tweets_count=count,
#                                        non_analyzed_comments_count=len(failed_comments),
#                                        total_retweets_count=total_retweets_count,
#                                        total_comment_analysis_accuracy=comment_analysis_accuracy)
#     report.save()
    return emv_timeline


@api_view(['GET', ])
def analyze_user(request, user_id, count):
    if request.method == 'GET':
        report = None
        serializer = None
        if _is_force(request):
            report = analyze_and_save_user(user_id, count)
            serializer = serializers.UserReport(report)
        else:
            try:
                report = models.UserAnalyzeReport.objects.filter(user_id=user_id)
                serializer = serializers.UserReport(report, many=True)
            except models.UserAnalyzeReport.DoesNotExist:
                report = analyze_and_save_user(user_id, count)
                serializer = serializers.UserReport(report)
        return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def js_timestamp(dt):
    """ Return a javascript timestamp from a datetime object.

        Javascript times are milliseconds since the epoch,
        whereas python uses just seconds, so lets return the
        times in the javascript format, so it's unambiguous to
        jqPlot.

    """
    return time.mktime(dt.timetuple()) * 1000


def date_handler(obj):
    return js_timestamp(obj) if isinstance(obj, datetime.datetime) else obj


@api_view(['GET', ])
def analyze_user_timeline(request, user_id, count):
    if request.method == 'GET':

        result = {}
        report = None
        serializer = None
        if _is_force(request):
            report = analyze_user_timeline_(user_id, count)
            result['output'] = report
        else:
            pass
#             try:
#                 report = models.UserAnalyzeReport.objects.filter(user_id=user_id)
#                 serializer = serializers.UserReport(report, many=True)
#             except models.UserAnalyzeReport.DoesNotExist:
#                 report = analyze_user_timeline_(user_id, count)
#                 serializer = serializers.UserReport(report)
        json_ = json.dumps(result, default=date_handler)
#         json_= "{\"output\": [[1410224291000.0, 33.8985], [1410224272000.0, 63.020625], [1410222419000.0, 83.05495833333333], [1410221744000.0, 93.072125]]}"
        print json_
        return Response(json_)

    return Response(serializers.errors, status=status.HTTP_400_BAD_REQUEST)
