# coding=utf-8
'''
Created on 07 Aug 2014

@author: ekondrashev
'''
from rest_framework import serializers
from models import TweetAnalyzeReport
from models import UserAnalyzeReport


class TweetReport(serializers.ModelSerializer):
    class Meta:
        model = TweetAnalyzeReport
        fields = ('start_time', 'end_time', 'tweet_id', 'algorithm', 'result', 'total_comments_count', 'non_analyzed_comments_count', 'comment_analysis_accuracy')


class UserReport(serializers.ModelSerializer):
    class Meta:
        model = UserAnalyzeReport
        fields = ('start_time', 'end_time', 'user_id', 'algorithm', 'result', 'total_tweets_count', 'total_retweets_count', 'non_analyzed_comments_count', 'total_comment_analysis_accuracy')