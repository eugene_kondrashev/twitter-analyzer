from django.conf.urls import patterns, url
from views import analyze_tweet, analyze_user, analyze_user_timeline

urlpatterns = patterns('',
    url(r'^analyze_tweet/(?P<user_id>.+?)/(?P<id_>[0-9]+)$', analyze_tweet, name='analyze_tweet'),
    url(r'^analyze_user/(?P<user_id>.+?)/(?P<count>[0-9]+)$', analyze_user, name='analyze_user'),
    url(r'^analyze_user_timeline/(?P<user_id>.+?)/(?P<count>[0-9]+)$', analyze_user_timeline, name='analyze_user_timeline'),
)
