from django.db import models


ALGORITHMS = (('Default', 'default'), )


class TweetAnalyzeReport(models.Model):
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    tweet_id = models.BigIntegerField()
    algorithm = models.CharField(choices=ALGORITHMS, default='default', max_length=50)
    result = models.FloatField()
    total_comments_count = models.BigIntegerField()
    non_analyzed_comments_count = models.BigIntegerField()
    comment_analysis_accuracy = models.FloatField()


class UserAnalyzeReport(models.Model):
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    user_id = models.CharField(max_length=50)
    algorithm = models.CharField(choices=ALGORITHMS, default='default', max_length=50)
    result = models.FloatField()
    total_tweets_count = models.BigIntegerField()
    total_retweets_count = models.IntegerField()
    non_analyzed_comments_count = models.BigIntegerField()
    total_comment_analysis_accuracy = models.FloatField()

