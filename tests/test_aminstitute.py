# coding=utf-8
'''
Created on Jul 10, 2014

@author: ekondrashev
'''
import unittest
from aminstitute import parse_emv


OUTPUT1 = '''


<html>

<head>

<title>Advanced Marketing Institute - Headline Analysis Results</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../ilovepalmtrees.css" type="text/css">

</head>



<body text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<!-- Start Header -->

<table width="100%" border="0" cellspacing="0" cellpadding="0">

  <tr>

    <td bgcolor="#19175B">

      <table width="760" border="0" cellspacing="0" cellpadding="0">

        <tr>

          <td width="295"><img src="/i/h_amilogo.gif" width="304" height="40"></td>

          <td width="154"><img src="/i/h_headlinemoney.gif" width="210" height="40"></td>

          <td width="311">

            <table width="100%" border="0" cellspacing="0" cellpadding="5">

              <form method="get" action="http://search.atomz.com/search/">

                <tr>

                  <td>

                    <div align="right" class="htext"><b><font color="#FFFFFF">Search:

                      </font></b>

                      <input type="text" name="sp-q" class="htext" size="15">

                      <input type="submit" name="Submit" value="Search!" class="stext">

                    </div>

                  </td>

                  <input type=hidden name="sp-a" value="sp1002bc4c">

                  <input type=hidden name="sp-p" value="all">

                  <input type=hidden name="sp-f" value="ISO-8859-1">

                </tr>

              </form>

            </table>

          </td>

        </tr>

      </table>

    </td>

  </tr>

  <tr>

    <td bgcolor="#EEEEEE"><img src="/i/c_transfiller.gif" width="25" height="3"></td>

  </tr>

  <tr>

    <td bgcolor="#7694D0">

      <!-- Begin Nav Nav Hot Stuff -->

      <table border="0" cellspacing="0" cellpadding="0" height="20">

        <tr bgcolor="#006699">

          <td class="stext" width="67" bgcolor="#3366CC" onMouseOver="this.style.backgroundColor='#7694D0';" onMouseOut="this.style.backgroundColor='#3366CC';" height="19">

            <div align="center"><b><a href="http://www.aminstitute.com/"><font color="#FFFFFF">Home</font></a></b></div>

          </td>

          <td class="stext" width="1" bgcolor="#3366CC" height="19"><img src="/i/n_spcron.gif" width="1" height="22"></td>

          <td class="stext" width="150" bgcolor="#3366CC" onMouseOver="this.style.backgroundColor='#7694D0';" onMouseOut="this.style.backgroundColor='#3366CC';" height="19">

            <div align="center"><b><a href="http://www.aminstitute.com/sales/"><font color="#FFFFFF">Sales

              &amp; Marketing</font></a></b></div>

          </td>

          <td class="stext" bgcolor="#3366CC" width="1" height="19"><img src="/i/n_spcron.gif" width="1" height="22"></td>

          <td class="stext" width="108" bgcolor="#3366CC" onMouseOver="this.style.backgroundColor='#7694D0';" onMouseOut="this.style.backgroundColor='#3366CC';" height="19">

            <div align="center"><b><a href="http://www.aminstitute.com/copywriting/"><font color="#FFFFFF">Copywriting</font></a></b></div>

          </td>

          <td class="stext" bgcolor="#BAC8E3" width="1" height="19"><img src="/i/c_transfiller.gif" width="1" height="22"></td>

          <td class="stext" width="150" bgcolor="#BAC8E3" height="19">

            <div align="center"><b><a href="#"><font color="#000000">Headline

              Analyzer</font></a></b></div>

          </td>

          <td class="stext" bgcolor="#BAC8E3" width="1" height="19"><img src="/i/c_transfiller.gif" width="1" height="22"></td>

          <td class="stext" width="103" bgcolor="#3366CC" onMouseOver="this.style.backgroundColor='#7694D0';" onMouseOut="this.style.backgroundColor='#3366CC';" height="19">

            <div align="center"><b><a href="http://www.aminstitute.com/newsletter/"><font color="#FFFFFF">Newsletter</font></a></b></div>

          </td>

          <td class="stext" bgcolor="#3366CC" width="1" height="19"><img src="/i/n_spcron.gif" width="1" height="22"></td>

          <td class="stext" width="91" bgcolor="#3366CC" onMouseOver="this.style.backgroundColor='#7694D0';" onMouseOut="this.style.backgroundColor='#3366CC';" height="19">

            <div align="center"><b><a href="http://www.aminstitute.com/about/"><font color="#FFFFFF">About

              Us</font></a></b></div>

          </td>

          <td class="stext" bgcolor="#3366CC" width="1" height="19"><img src="/i/n_spcron.gif" width="1" height="22"></td>

          <td class="stext" width="85" bgcolor="#3366CC" onMouseOver="this.style.backgroundColor='#7694D0';" onMouseOut="this.style.backgroundColor='#3366CC';" height="19">

            <div align="center"><b><a href="http://www.aminstitute.com/about/contact.htm"><font color="#FFFFFF">Contact</font></a></b></div>

          </td>

        </tr>

      </table>

      <!-- End Nav Nav Hot Stuff -->

    </td>

  </tr>

  <tr>

    <td bgcolor="#FFFFFF"><img src="/i/c_transfiller.gif" width="25" height="1"></td>

  </tr>

  <tr>

    <td bgcolor="#BAC8E3"><img src="/i/c_transfiller.gif" width="25" height="7"></td>

  </tr>

  <tr>

    <td><img src="/i/c_transfiller.gif" width="25" height="1"></td>

  </tr>

</table>

<!-- End Header -->

<table width="100%" border="0" cellspacing="0" cellpadding="0">

  <tr>

    <td class="htext" valign="top" bgcolor="#EFEFEF" width="135">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class="htext" bgcolor="#EFEFEF" height="20">&nbsp;<b>Free Analyzer
            </b></td>
        </tr>
        <tr>
          <td class="stext" bgcolor="#FFFFFF"><img src="../i/c_transfiller.gif" width="25" height="1"></td>
        </tr>
        <tr>
          <td class="htext" bgcolor="#BAC8E3" onMouseOver="this.style.backgroundColor='#DFDFDF';" onMouseOut="this.style.backgroundColor='#BAC8E3';" height="20">&nbsp;<a class="nav2" href="http://www.aminstitute.com/headline/about.htm">How
            it Works</a></td>
        </tr>
        <tr>
          <td class="htext" bgcolor="#FFFFFF" height="1"><img src="../i/c_transfiller.gif" width="25" height="1"></td>
        </tr>
      </table>
      <br>

      <table width="100%" border="0" cellspacing="0" cellpadding="5">

        <tr class="htext">

          <td><b>Stay Updated!<br>

            </b>Sign up for our newsletter and we'll let you know when new copy

            analysis tools are available!<br>

            <a href="../newsletter/index.htm"><b>Subscribe Here</b></a></td>

        </tr>

      </table>

    </td>

    <td bgcolor="#FFFFFF" valign="top">

      <table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr class="htext">

          <td bgcolor="#FFFFCC" height="41"><b class="ntext">&nbsp;Free Headline

            Analysis Results</b></td>

        </tr>

      </table>

      <table width="625" border="0" cellspacing="0" cellpadding="15">

        <tr class="htext">

          <td>

            <p>Thanks for using Advanced Marketing Institute's Headline Analyzer!
              You submitted the following headline for Emotional Marketing Value
              analysis:</p>
            <p><b><font size="4">"ready to upload consciousness into smartwatch Yes"</font></b></p>

            <table border="0" cellspacing="3" cellpadding="5" width="353" align="center">
              <tr bgcolor="#EFEFEF">
                <td class="htext" width="65%">
                  <p><b> Your Headline's <a href="about.htm">EMV Score</a>:</b></p>                  </td>
                <td class="htext" width="35%" valign="top" bgcolor="#BAC8E3">
                  <div align="center"><font size="7"><b><font size="5">57.14%</font></b></font></div>                </td>
              </tr>
            </table>

            <p>This score indicates that your headline has a total of 57.14%
              Emotional Marketing Value (EMV) Words. To put that in perspective,
              the English language contains approximately 20% EMV words.</p>
            <p>And for comparison, most professional copywriters' headlines will
              have <b>30%-40% EMV Words</b> in their headlines, while the most
              gifted copywriters will have <b>50%-75%</b> EMV words in headlines.</p>
            <p>A perfect score would be 100%, but that is rare unless your headline
              is less than five words.</p>
            <p>While the overall EMV score for your headline is 57.14%, your
              headline also has the following predominant emotion classification:</p>
            <br>


    <table width="100%" border="0" cellspacing="3" cellpadding="5">
      <tr>
        <td width="33%" class="artext13" bgcolor="#6539B7">
          <p align="center"><b><font color="#FFFFFF">Spiritual</font></b></p>
        </td>
      </tr>
      <tr>
        <td width="33%" class="htext" bgcolor="#D2D2F0">

      <p>
      Your headline carries words that have a predominantly Spiritual appeal. Words that resonate with Spiritual impact are the smallest number of words in the language. AMI research has found that Spiritual impact words carry the strongest potential for influence and often appeal to people at a very deep emotional level.
      </p>
      <p>
      Words with Spiritual impact are best used with people and businesses desiring to make an appeal to some aspect of spirituality. This does not mean religion specifically, but any product or service that resonates with “spirituality” oriented markets are appropriate. The clergy, new age, health food and related markets all respond favorably to sales copy heavy with Spiritual impact content. Women and children also respond strongly to words in the Spiritual sphere. Marketing documents with strong Spiritual impact content can make for the most powerful presentations in the marketplac,e but must be used with considerable skill.
      </p>

        </td>
      </tr>
    </table>


            <p align="center"><b><a href="http://www.aminstitute.com/headline/"><br>

              Analyze Another Headline &gt;</a></b></p>            </td>

          <td width="120" valign="top">
          <script type="text/javascript"><!--
google_ad_client = "pub-2049674145629799";
google_ad_width = 120;
google_ad_height = 600;
google_ad_format = "120x600_as";
google_ad_type = "text";
google_ad_channel ="4999562354";
google_color_border = "FFFFFF";
google_color_bg = "FFFFFF";
google_color_link = "0000FF";
google_color_url = "008000";
google_color_text = "000000";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
          </td>
        </tr>
      </table>



    </td>

  </tr>

</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">

  <tr>

    <td bgcolor="#BAC8E3">

      <table width="760" border="0" cellspacing="0" cellpadding="3">

        <tr bgcolor="#999999">

          <td class="stext" width="80%" bgcolor="#7694D0"><font color="#FFFFFF"><a href="http://www.aminstitute.com/"><font color="#FFFFFF">Home</font></a>

            | <a href="http://www.aminstitute.com/media/"><font color="#FFFFFF">Info

            For Media</font></a> | <a href="http://www.aminstitute.com/about/privacy.htm"><font color="#FFFFFF">Privacy

            Policy</font></a> | <a href="http://www.aminstitute.com/about/tou.htm"><font color="#FFFFFF">Terms

            of Use</font></a> | <a href="http://www.aminstitute.com/about/contact.htm"><font color="#FFFFFF">Contact

            Us</font></a></font></td>

          <td class="stext" width="20%" bgcolor="#7694D0">

            <div align="right"><font color="#FFFFFF">^</font> <a href="#top" class="blue">Back

              to Top</a>&nbsp;</div>

          </td>

        </tr>

      </table>

    </td>

  </tr>

  <tr>

    <td bgcolor="#DFDFDF">

      <table width="760" border="0" cellspacing="0" cellpadding="3">

        <tr>

          <td bgcolor="#C9C9C9" colspan="2" height="19">

            <div align="center"><b class="htext"><font color="#666666">&copy;

              2006 Advanced Marketing Institute - All Rights Reserved</font></b></div>

          </td>

        </tr>

      </table>

    </td>

  </tr>

  <tr>

    <td>

      <table width="760" border="0" cellspacing="0" cellpadding="3">

        <tr>

          <td colspan="2" height="19">

            <div align="center" class="stext">Advanced Marketing Institute is

              owned and operated by <a href="http://www.imcuniverse.com/">IMC

              Digital Universe</a>.</div>

          </td>

        </tr>

      </table>

    </td>

  </tr>

</table>

<p>&nbsp;</p>

<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-616964-4";
urchinTracker();
</script>

</body>

</html>


'''
class Test(unittest.TestCase):


    def test_parse_emv(self):
        actual = parse_emv(OUTPUT1)
        self.assertEqual(0.5714, actual)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_parse_emv']
    unittest.main()